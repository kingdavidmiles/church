import ApiService from "@/core/services/api.service";

import JwtService from "@/core/services/jwt.service";
import { fb } from "@/core/firebase";

// action types
export const VERIFY_AUTH = "verifyAuth";
export const LOGIN = "login";
export const LOGOUT = "logout";
export const REGISTER = "register";
export const UPDATE_PASSWORD = "updateUser";

// mutation types
export const PURGE_AUTH = "logOut";
export const SET_AUTH = "setUser";
export const SET_PASSWORD = "setPassword";
export const SET_ERROR = "setError";

const state = {
  errors: null,
  user: {},
  isAuthenticated: !!JwtService.getToken(),
};

const getters = {
  currentUser(state) {
    return state.user;
  },
  isAuthenticated(state) {
    return state.isAuthenticated;
  },
};

const actions = {
  [LOGIN](context, credentials) {
    return new Promise((resolve) => {
      const { email, password } = credentials;
      fb.auth()
        .signInWithEmailAndPassword(email, password)
        .then((userCredential) => {
          // Signed in
          const user = userCredential.user;
          const payload = {
            errors: null,
            user,
            isAuthenticated: user.refreshToken,
          };
          // console.log("Here what post returns", data);
          context.commit(SET_AUTH, payload);
          resolve(payload);
        })
        .catch((error) => {
          const errorCode = error.code;
          const errorMessage = error.message;
          console.error(error);
          context.commit(SET_ERROR, errorMessage);
        });
    });
  },
  [LOGOUT](context) {
    fb.auth()
      .signOut()
      .then(() => {
        // Sign-out successful.
        context.commit(PURGE_AUTH);
      })
      .catch((error) => {
        // An error happened.
      });
  },
  [REGISTER](context, credentials) {
    return new Promise((resolve) => {
      const { email, password } = credentials;
      fb.auth()
        .createUserWithEmailAndPassword(email, password)
        .then((userCredential) => {
          const user = userCredential.user;
          const payload = {
            errors: null,
            user,
            isAuthenticated: user.refreshToken,
          };

          let newUser = fb
            .firestore()
            .collection("users")
            .doc(user.uid)
            .set({
              email: email,
              password: password,
            });
          context.commit(SET_AUTH, payload);
          resolve(payload);
        })

        .catch((error) => {
          const errorCode = error.code;
          const errorMessage = error.message;
          console.error(error);
          context.commit(SET_ERROR, errorMessage);
        });
    });
  },
  [VERIFY_AUTH](context) {
    if (JwtService.getToken()) {
      ApiService.setHeader();
      ApiService.get("verify")
        .then(({ data }) => {
          context.commit(SET_AUTH, data);
        })
        .catch((error) => {
          const errorCode = error.code;
          const errorMessage = error.message;
          console.error(error);
          context.commit(SET_ERROR, errorMessage);
        });
    } else {
      context.commit(PURGE_AUTH);
    }
  },
  [UPDATE_PASSWORD](context, payload) {
    const password = payload;

    return ApiService.put("password", password).then(({ data }) => {
      context.commit(SET_PASSWORD, data);
      return data;
    });
  },
};

const mutations = {
  [SET_ERROR](state, error) {
    state.errors = error;
  },
  [SET_AUTH](state, user) {
    state.isAuthenticated = true;
    state.user = user;
    state.errors = {};
    JwtService.saveToken(state.user.token);
  },
  [SET_PASSWORD](state, password) {
    state.user.password = password;
  },
  [PURGE_AUTH](state) {
    state.isAuthenticated = false;
    state.user = {};
    state.errors = {};
    JwtService.destroyToken();
  },
};

export default {
  state,
  actions,
  mutations,
  getters,
};
