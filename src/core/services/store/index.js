import Vue from "vue";
import Vuex from "vuex";
import { fb } from "../../firebase";
import VuexEasyFirestore from "vuex-easy-firestore";

import auth from "./auth.module";
import htmlClass from "./htmlclass.module";
import config from "./config.module";
import breadcrumbs from "./breadcrumbs.module";
import profile from "./profile.module";
import users from "./users.module";
import newsFeedmodule from "./newsFeed.module";
import sites from "./sites.module";
Vue.use(Vuex);

// do the magic 🧙🏻‍♂️
const easyFirestore = VuexEasyFirestore([users, newsFeedmodule, sites], {
  logging: true,
  FirebaseDependency: fb,
});

const store = new Vuex.Store({
  plugins: [easyFirestore],
  modules: {
    auth,
    htmlClass,
    config,
    breadcrumbs,
    profile,
    newsFeedmodule,
    sites,
  },
});

export default store;
