//@ts-check

export default {
  state: {
    siteInfo: {},
  },
  serverChange: {
    convertTimestamps: {
      created_at: "%convertTimestamp%", // default
      updated_at: "%convertTimestamp%", // default
    },
  },

  firestorePath: "sites/{siteId}",
  firestoreRefType: "doc",
  moduleName: "sitesModule",
  statePropName: "siteInfo",
};
