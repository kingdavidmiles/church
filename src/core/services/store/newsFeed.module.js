export default {
  state: {
    news: {},
  },
  serverChange: {
    convertTimestamps: {
      created_at: "%convertTimestamp%", // default
      updated_at: "%convertTimestamp%", // default
    },
  },

  firestorePath: "sites/{siteId}/news",
  firestoreRefType: "collection",

  moduleName: "newsModule",
  statePropName: "news",

  namespaced: true, // automatically added

  sync: {
    debounceTimerMs: 2000, // 2 seconds for slow networks
  },
};
