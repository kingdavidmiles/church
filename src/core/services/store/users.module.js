export const UsersModule = {
  firestorePath: "users/{userId}",
  firestoreRefType: "doc",
  statePropName: "user",
  moduleName: "usersModule",
  state: {},
  actions: {},
  getters: {},
};
export default UsersModule;
