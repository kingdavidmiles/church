import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import "firebase/functions";
import "firebase/messaging";
import "firebase/analytics";
import "firebase/database";
import "firebase/storage";
// import 'firebase/app-check';

const config = {
  apiKey: "AIzaSyAQ2cJkRfMtwdYinosJ0rlM-qKaC7xFnOQ",
  authDomain: "yilla-3784b.firebaseapp.com",
  databaseURL: "https://yilla-3784b-default-rtdb.firebaseio.com",
  projectId: "yilla-3784b",
  storageBucket: "yilla-3784b.appspot.com",
  messagingSenderId: "448813703522",
  appId: "1:448813703522:web:e5f789fcdc6c03a46e4c3b",
  measurementId: "G-Q5P14SLW0L",
};

let firebaseApp = firebase.initializeApp(config);

// throwing error, open issue https://github.com/firebase/firebase-js-sdk/pull/5055
// const appCheck = firebaseApp.appCheck();
// appCheck.activate(process.env.VUE_APP_APPCHECK_PUBLIC_KEY);

const firebaseAuth = firebaseApp.auth();
const auth = firebaseApp.auth();
const db = firebaseApp.firestore();
const fns = firebaseApp.functions();
const rt = firebaseApp.database(process.env.VUE_APP_FIREBASE_DATABASE_URL);
const presence = rt; //firebaseApp.database(process.env.VUE_APP_FIREBASE_PRESENCE_DB);
const analytics = firebaseApp.analytics();
const installs = firebaseApp.installations();
const storage = firebaseApp.storage();

//Setup emulators
if (process.env.NODE_ENV === "development") {
  if (process.env.VUE_APP_USE_EMULATORS === "ALL") {
    fns.useEmulator("localhost", 5003);
    auth.useEmulator("http://localhost:9099/");
    storage.useEmulator("localhost", 9199);
    db.useEmulator("localhost", 8082);
    rt.useEmulator("localhost", 4001);
    presence.useEmulator("localhost", 4001);

    // eslint-disable-next-line no-console
    console.log("Using All Emulators");
  }

  if (process.env.VUE_APP_USE_EMULATORS === "FNS") {
    fns.useEmulator("localhost", 5003);
    // eslint-disable-next-line no-console
    console.log("Using Functions Emulator");
  }
}

export { firebaseAuth, auth, db, fns, rt, analytics, installs, storage, presence };

let fcm = null;

try {
  fcm = firebaseApp.messaging();
  fcm.usePublicVapidKey(`${process.env.VUE_APP_MESSAGING_KEY}`);
} catch (err) {
  // eslint-disable-next-line no-console
  console.debug("Messaging error ", err);
}

export const messaging = fcm;

export const FbInit = () => {
  return new Promise((resolve, reject) => {
    if (process.env.NODE_ENV !== "development") {
      firebase.firestore().settings({
        cacheSizeBytes: firebase.firestore.CACHE_SIZE_UNLIMITED,
        merge: true,
      });
      firebase
        .firestore()
        .enablePersistence({
          synchronizeTabs: true,
        })
        .then(() => {
          return resolve({
            firebaseApp,
            db,
            firebaseAuth,
          });
        })
        .catch((err) => {
          if (err.code === "failed-precondition") {
            reject(err);
            // Multiple tabs open, persistence can only be
            // enabled in one tab at a a time.
          } else if (err.code === "unimplemented") {
            reject(err);
            // The current browser does not support all of
            // the features required to enable persistence
          }
        });
    }

    return resolve({
      firebaseApp,
      db,
      firebaseAuth,
    });
  });
};

//Firebase for vuex-easy-firestore
export const fb = firebase;
