import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      //
      path: "/home",

      component: () => import("@/view/pages/Home.vue"),
      children: [
        {
          path: "/home",
          name: "home",
          component: () => import("@/view/pages/Home.vue"),
        },

        //
      ],
    },

    {
      path: "/",
      redirect: "/home",
      component: () => import("@/view/pages/Paraschat.vue"),
      children: [
        {
          path: "/paraschat",
          name: "Paraschat",
          component: () => import("@/view/pages/Paraschat.vue"),
        },
      ],
    },
    {
      path: "/",
      redirect: "/home",
      component: () => import("./view/pages/DonationList/ChurchRenovation.vue"),
      children: [
        {
          path: "/synagogue_renovation",
          name: "synagogue_renovation",
          component: () => import("./view/pages/DonationList/ChurchRenovation.vue"),
        },
      ],
    },
    {
      path: "/",
      redirect: "/home",
      component: () => import("./view/pages/DonationList/FeedThePeople.vue"),
      children: [
        {
          path: "/feed_the_people",
          name: "feed_the_people",
          component: () => import("./view/pages/DonationList/FeedThePeople.vue"),
        },
      ],
    },
    {
      path: "/",
      redirect: "/home",
      component: () => import("./view/pages/DonationList/HelpTheChildren.vue"),
      children: [
        {
          path: "/help_the_children",
          name: "help_the_children",
          component: () => import("./view/pages/DonationList/HelpTheChildren.vue"),
        },
      ],
    },
    {
      path: "/",
      redirect: "/home",
      component: () => import("./view/pages/DonationList/index.vue"),
      children: [
        {
          path: "/donation_list",
          name: "donation_list",
          component: () => import("./view/pages/DonationList/index.vue"),
        },
      ],
    },

    //

    //      _       _           _             _           _     _                         _
    //     / \   __| |_ __ ___ (_)_ __     __| | __ _ ___| |__ | |__   ___   __ _ _ __ __| |
    //    / _ \ / _` | '_ ` _ \| | '_ \   / _` |/ _` / __| '_ \| '_ \ / _ \ / _` | '__/ _` |
    //   / ___ \ (_| | | | | | | | | | | | (_| | (_| \__ \ | | | |_) | (_) | (_| | | | (_| |
    //  /_/   \_\__,_|_| |_| |_|_|_| |_|  \__,_|\__,_|___/_| |_|_.__/ \___/ \__,_|_|  \__,_|

    // Admin
    {
      path: "/",
      redirect: "/home",
      component: () => import("@/view/layout/Layout"),
      children: [
        {
          path: "/admin/dashboard",
          name: "dashboard",
          component: () => import("@/view/pages/Dashboard.vue"),
        },
        {
          path: "/news_feed",
          name: "NewsFeed",
          component: () => import("./view/pages/Admin/newsFeed/newsFeeds.vue"),
        },
      ],
    },
    // Admin:: end here
    {
      path: "/custom-error",
      name: "error",
      component: () => import("@/view/pages/error/Error.vue"),
      children: [
        {
          path: "error-1",
          name: "error-1",
          component: () => import("@/view/pages/error/Error-1.vue"),
        },
      ],
    },
    // Admin

    {
      path: "/",
      component: () => import("@/view/pages/auth/login_pages/Login-1"),
      children: [
        {
          name: "login",
          path: "/login",
          component: () => import("@/view/pages/auth/login_pages/Login-1"),
        },
        {
          name: "register",
          path: "/register",
          component: () => import("@/view/pages/auth/login_pages/Login-1"),
        },
      ],
    },
    {
      path: "*",
      redirect: "/404",
    },
    {
      // the 404 route, when none of the above matches
      path: "/404",
      name: "404",
      component: () => import("@/view/pages/error/Error-1.vue"),
    },
  ],
});
