import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "@/core/services/store";
import ApiService from "@/core/services/api.service";
import MockService from "@/core/mock/mock.service";
import { VERIFY_AUTH } from "@/core/services/store/auth.module";
import { RESET_LAYOUT_CONFIG } from "@/core/services/store/config.module";
import AOS from "aos";
import "aos/dist/aos.css";
import VueSnip from "vue-snip";
import { fb } from "@/core/firebase";

Vue.use(VueSnip);
Vue.config.productionTip = false;

// Global 3rd party plugins
import "popper.js";
import "tooltip.js";
import PerfectScrollbar from "perfect-scrollbar";
window.PerfectScrollbar = PerfectScrollbar;
import ClipboardJS from "clipboard";
window.ClipboardJS = ClipboardJS;

// Vue 3rd party plugins
import i18n from "@/core/plugins/vue-i18n";
import vuetify from "@/core/plugins/vuetify";
import "@/core/plugins/portal-vue";
import "@/core/plugins/bootstrap-vue";
import "@/core/plugins/perfect-scrollbar";
import "@/core/plugins/highlight-js";
import "@/core/plugins/inline-svg";
import "@/core/plugins/apexcharts";
import "@/core/plugins/metronic";
import "@/core/plugins/treeselect";
import "@mdi/font/css/materialdesignicons.css";

// API service init
ApiService.init();

// Remove this to disable mock API
MockService.init();

// Be sure to initialise Firebase first!
fb.auth().onAuthStateChanged((user) => {
  if (user) {
    // user is logged in so openDBChannel
    const { isAnonymous, uid, displayName, email, emailVerified } = user;
    store.dispatch("usersModule/fetchAndAdd").catch(console.error);
    store
      .dispatch("usersModule/patch", {
        id: uid,
        email,
        displayName,
        isAnonymous,
        emailVerified,
      })
      .catch(console.error);
    // or fetchAndAdd
    // store.dispatch('userData/fetchAndAdd')
  }
});
//
router.beforeEach((to, from, next) => {
  // Ensure we checked auth before each page load.
  Promise.all([store.dispatch(VERIFY_AUTH)]).then(next);

  // reset config to initial state
  store.dispatch(RESET_LAYOUT_CONFIG);

  // Scroll page to top on every route change
  setTimeout(() => {
    window.scrollTo(0, 0);
  }, 100);
});

//
Vue.mixin({
  async mounted() {
    // Mixins for pathVars
    const genPath = {
      siteId: this.siteId,
    };
    await this.$store.dispatch("sitesModule/setPathVars", genPath);
    await this.$store.dispatch("newsModule/setPathVars", genPath);
    // await this.$store.dispatch("withdrawalsModule/setPathVars", genPath)

    // always fetch the sites details
    const query = {
      pathVariables: { siteId: this.siteId },
      clauses: {
        where: [["siteId", "==", this.siteId]],
      },
    };
    await this.$store.dispatch("sitesModule/fetchAndAdd", query);
  },
  computed: {
    siteInfo() {
      return this.$store.state.sitesModule.siteInfo;
    },
    user() {
      return this.$store.state.usersModule.user;
    },
  },
  data() {
    return {
      // TODO: move to .env
      siteId: "yilla",
    };
  },
});
new Vue({
  router,
  store,
  i18n,
  vuetify,
  render: (h) => h(App),
}).$mount("#app", AOS.init());
